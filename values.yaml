# Common Chart parameters

# -- String to partially override nextcloud.fullname
nameOverride: ""
# -- String to fully override nextcloud.fullname
fullnameOverride: ""

# OpenProject

image:
  # -- OpenProject image repository
  repository: openproject/community
  # -- OpenProject image tag
  # @default -- Chart.appVersion
  tag: ""
  # -- OpenProject image pull policy
  pullPolicy: IfNotPresent

# -- Seconds OpenProject pod needs to terminate gracefully
terminationGracePeriodSeconds: ""

startupProbe:
  # -- Enable startupProbe on OpenProject containers
  enabled: true
  # -- Initial delay seconds for startupProbe
  initialDelaySeconds: 20
  # -- Period seconds for startupProbe
  periodSeconds: 5
  # -- Timeout seconds for startupProbe
  timeoutSeconds: 5
  # -- Failure threshold for startupProbe
  failureThreshold: 5
  # -- Success threshold for startupProbe
  successThreshold: 1

readinessProbe:
  # -- Enable readinessProbe on OpenProject containers
  enabled: true
  # -- Initial delay seconds for readinessProbe
  initialDelaySeconds: 20
  # -- Period seconds for readinessProbe
  periodSeconds: 5
  # -- Timeout seconds for readinessProbe
  timeoutSeconds: 2
  # -- Failure threshold for readinessProbe
  failureThreshold: 5
  # -- Success threshold for readinessProbe
  successThreshold: 1

livenessProbe:
  # -- Enable livenessProbe on OpenProject Primary containers
  enabled: true
  # -- Initial delay seconds for livenessProbe
  initialDelaySeconds: 20
  # -- Period seconds for livenessProbe
  periodSeconds: 5
  # -- Timeout seconds for livenessProbe
  timeoutSeconds: 6
  # -- Failure threshold for livenessProbe
  failureThreshold: 5
  # -- Success threshold for livenessProbe
  successThreshold: 1

resources:
  requests:
    # -- The requested memory for the OpenProject containers
    memory: 256Mi
    # -- The requested cpu for the OpenProject containers
    cpu: 250m
  limits:
    # -- The memory limits for the OpenProject containers
    memory: 1024Mi
    # -- The cpu limits for the OpenProject containers
    cpu: 500m


# Database

database:
  # The configuration only allows Postgresql databases
  # -- External database service
  host: postgresql.database.svc.cluster.local
  # -- Port of the external database
  port: 5432
  existingSecret:
    # -- Name of an existing secret containing the database credentials
    name: database-credentials
    # -- Key containing the database name
    databaseKey: database
    # -- Key containing the database user
    userKey: user
    # -- Key containing the user password
    passwordKey: password


# Persistence

persistence:
  # -- PVC Access Mode for OpenProject uploads volume
  accessMode: ReadWriteOnce
  # -- PVC StorageClass for OpenProject uploads volume
  storageClassName: retain-local-path
  # -- PVC Storage Request for OpenProject uploads volume
  size: 20Gi


# OpenProject configuration

config:
  # -- Enable hsts
  hsts: "true"
  # -- Modify PostgreSQL statement timout.
  # Increase in case you get errors such as "ERROR: canceling statement due to statement timeout".
  #
  # https://www.openproject.org/docs/installation-and-operations/configuration/environment/#postgresql-statement_timeout
  #
  postgresStatementTimeout: 120s
  oidc:
    # -- Enable OICD
    enabled: false
    # -- Provider
    provider: "Keycloak"
    # -- Hostname
    host: "auth.mathezirkel-augsburg.de"
    # -- Client ID
    identifier: "openproject-auth"
    # -- Client secret
    existingSecret:
      name: openproject-client-secret
      key: secret
    # -- Issuer
    issuer: "https://auth.mathezirkel-augsburg.de/realms/mathezirkel"
    # -- Authorization endpoint
    authorizationEndpoint: "/realms/mathezirkel/protocol/openid-connect/auth"
    # -- Token endpoint
    tokenEndpoint: "/realms/mathezirkel/protocol/openid-connect/token"
    # -- Userinfo endpoint
    userinfoEndpoint: "/realms/mathezirkel/protocol/openid-connect/userinfo"
    # -- Logout endpoint
    endSessionEndpoint: "https://auth.mathezirkel-augsburg.de/realms/mathezirkel/protocol/openid-connect/logout"


# Service

service:
  # -- OpenProject service port
  port: 8080


# Service account

serviceAccount:
  # -- Enable creation of ServiceAccount for OpenProject pod.
  # If set to false, the default serviceAccount is used.
  create: true
  # -- The name of the service account to use.
  # @default -- openproject.fullname
  name: ""


# Ingress

ingress:
  # -- Enable the ingress
  enabled: true
  # -- List of traefik entrypoints to listen
  entryPoints:
    - websecure
  # -- Hostname of the OpenProject instance
  host: openproject.mathezirkel-augsburg.de
  tls:
    # -- Name of the certResolver configured via traefik
    certResolver: lets-encrypt


# Cache

memcached:
  # -- When set to true, a memcached will be deployed into current namespace, when false there is no cache
  bundled: true
