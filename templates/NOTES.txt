OpenProject was successfully installed!

{{- if .Values.ingress.enabled }}
You can access it via https://{{ .Values.ingress.host }}
{{- end }}

Summary:
--------
OpenProject: {{ .Values.image.tag }}
Memcached: {{ if .Values.memcached.bundled }}{{ .Values.memcached.image.tag }}{{ else }}not available{{ end }}
