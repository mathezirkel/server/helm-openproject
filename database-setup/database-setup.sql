-- Add user for postgresql
CREATE USER openprojectuser WITH PASSWORD 'CHANGEME';

-- Add database for nextcloud
CREATE DATABASE openproject WITH OWNER=openprojectuser;
