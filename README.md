# OpenProject Helm chart

![Version: 1.0.0](https://img.shields.io/badge/Version-1.0.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 14.2](https://img.shields.io/badge/AppVersion-14.2-informational?style=flat-square)

This repository contains a [Helm](https://helm.sh/) chart to deploy [OpenProject](https://www.openproject.org) on Kubernetes.
OpenProject is an open-source project management software for project planning, tracking, and team collaboration.

**Please note that currently, there are no plans to deploy this Helm chart, nor has the chart been tested.**

We use the official docker image of [OpenProject Community](https://hub.docker.com/r/openproject/community).

## Maintainers

| Name | Email | Url |
| ---- | ------ | --- |
| Felix Stärk | <felix.staerk@posteo.de> |  |
| Sven Prüfer | <sven@musmehl.de> |  |

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami | memcached | ^6.3.9 |

## Installation and configuration

1. Create namespace.
    ```shell
    kubectl create namespace openproject
    ```

2. Create a secret for the openproject database crendentials:
    ```shell
    kubectl create secret generic database-credentials \
    --from-literal=database=openproject \
    --from-literal=user=openprojectuser \
    --from-literal=password=CHANGEME \
    -n openproject
    ```

3. Create a database user and a database in the internal database using the [script](./database-setup/database-setup.sql). Don't forget to modify the password.

4. Create a secret for memcached password:
    ```shell
    kubectl create secret generic memcached-credentials \
    --from-literal=memcached-password=CHANGEME \
    -n openproject
    ```

5. Adjust the configuration in [values.yaml](.values.yaml).

For a full documentation regarding the openproject configuration see https://www.openproject.org/docs/installation-and-operations/configuration/environment/.

5. Deploy the helm chart
   ```shell
   helm upgrade --install -n openproject openproject .
   ```

## Connecting Keycloak

Follow the instruction in the [official documentation](https://www.openproject.org/docs/installation-and-operations/misc/custom-openid-connect-providers/).

Create a secret for the Keycloak credentials:

```shell
kubectl create secret generic openproject-client-secret \
--from-literal=secret=CHANGEME \
-n openproject
```

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| config.hsts | string | `"true"` | Enable hsts |
| config.oidc.authorizationEndpoint | string | `"/realms/mathezirkel/protocol/openid-connect/auth"` | Authorization endpoint |
| config.oidc.enabled | bool | `false` | Enable OICD |
| config.oidc.endSessionEndpoint | string | `"https://auth.mathezirkel-augsburg.de/realms/mathezirkel/protocol/openid-connect/logout"` | Logout endpoint |
| config.oidc.existingSecret | object | `{"key":"secret","name":"openproject-client-secret"}` | Client secret |
| config.oidc.host | string | `"auth.mathezirkel-augsburg.de"` | Hostname |
| config.oidc.identifier | string | `"openproject-auth"` | Client ID |
| config.oidc.issuer | string | `"https://auth.mathezirkel-augsburg.de/realms/mathezirkel"` | Issuer |
| config.oidc.provider | string | `"Keycloak"` | Provider |
| config.oidc.tokenEndpoint | string | `"/realms/mathezirkel/protocol/openid-connect/token"` | Token endpoint |
| config.oidc.userinfoEndpoint | string | `"/realms/mathezirkel/protocol/openid-connect/userinfo"` | Userinfo endpoint |
| config.postgresStatementTimeout | string | `"120s"` | Modify PostgreSQL statement timout. Increase in case you get errors such as "ERROR: canceling statement due to statement timeout".  https://www.openproject.org/docs/installation-and-operations/configuration/environment/#postgresql-statement_timeout  |
| database.existingSecret.databaseKey | string | `"database"` | Key containing the database name |
| database.existingSecret.name | string | `"database-credentials"` | Name of an existing secret containing the database credentials |
| database.existingSecret.passwordKey | string | `"password"` | Key containing the user password |
| database.existingSecret.userKey | string | `"user"` | Key containing the database user |
| database.host | string | `"postgresql.database.svc.cluster.local"` | External database service |
| database.port | int | `5432` | Port of the external database |
| fullnameOverride | string | `""` | String to fully override nextcloud.fullname |
| image.pullPolicy | string | `"IfNotPresent"` | OpenProject image pull policy |
| image.repository | string | `"openproject/community"` | OpenProject image repository |
| image.tag | string | Chart.appVersion | OpenProject image tag |
| ingress.enabled | bool | `true` | Enable the ingress |
| ingress.entryPoints | list | `["websecure"]` | List of traefik entrypoints to listen |
| ingress.host | string | `"openproject.mathezirkel-augsburg.de"` | Hostname of the OpenProject instance |
| ingress.tls.certResolver | string | `"lets-encrypt"` | Name of the certResolver configured via traefik |
| livenessProbe.enabled | bool | `true` | Enable livenessProbe on OpenProject Primary containers |
| livenessProbe.failureThreshold | int | `5` | Failure threshold for livenessProbe |
| livenessProbe.initialDelaySeconds | int | `20` | Initial delay seconds for livenessProbe |
| livenessProbe.periodSeconds | int | `5` | Period seconds for livenessProbe |
| livenessProbe.successThreshold | int | `1` | Success threshold for livenessProbe |
| livenessProbe.timeoutSeconds | int | `6` | Timeout seconds for livenessProbe |
| memcached.bundled | bool | `true` | When set to true, a memcached will be deployed into current namespace, when false there is no cache |
| nameOverride | string | `""` | String to partially override nextcloud.fullname |
| persistence.accessMode | string | `"ReadWriteOnce"` | PVC Access Mode for OpenProject uploads volume |
| persistence.size | string | `"20Gi"` | PVC Storage Request for OpenProject uploads volume |
| persistence.storageClassName | string | `"retain-local-path"` | PVC StorageClass for OpenProject uploads volume |
| readinessProbe.enabled | bool | `true` | Enable readinessProbe on OpenProject containers |
| readinessProbe.failureThreshold | int | `5` | Failure threshold for readinessProbe |
| readinessProbe.initialDelaySeconds | int | `20` | Initial delay seconds for readinessProbe |
| readinessProbe.periodSeconds | int | `5` | Period seconds for readinessProbe |
| readinessProbe.successThreshold | int | `1` | Success threshold for readinessProbe |
| readinessProbe.timeoutSeconds | int | `2` | Timeout seconds for readinessProbe |
| resources.limits.cpu | string | `"500m"` | The cpu limits for the OpenProject containers |
| resources.limits.memory | string | `"1024Mi"` | The memory limits for the OpenProject containers |
| resources.requests.cpu | string | `"250m"` | The requested cpu for the OpenProject containers |
| resources.requests.memory | string | `"256Mi"` | The requested memory for the OpenProject containers |
| service.port | int | `8080` | OpenProject service port |
| serviceAccount.create | bool | `true` | Enable creation of ServiceAccount for OpenProject pod. If set to false, the default serviceAccount is used. |
| serviceAccount.name | string | openproject.fullname | The name of the service account to use. |
| startupProbe.enabled | bool | `true` | Enable startupProbe on OpenProject containers |
| startupProbe.failureThreshold | int | `5` | Failure threshold for startupProbe |
| startupProbe.initialDelaySeconds | int | `20` | Initial delay seconds for startupProbe |
| startupProbe.periodSeconds | int | `5` | Period seconds for startupProbe |
| startupProbe.successThreshold | int | `1` | Success threshold for startupProbe |
| startupProbe.timeoutSeconds | int | `5` | Timeout seconds for startupProbe |
| terminationGracePeriodSeconds | string | `""` | Seconds OpenProject pod needs to terminate gracefully |

## License

This repository is based on the work from the the official [helm chart](https://github.com/opf/helm-charts) which is licensed under the [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0).
The modifications and contents of this repository are licensed under [AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0), see [LICENSE](LICENSE).

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.11.2](https://github.com/norwoodj/helm-docs/releases/v1.11.2)
